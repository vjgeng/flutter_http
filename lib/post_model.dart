import 'package:flutter/foundation.dart';

class Post {
  final String q;
  final String a;
  final String h;


  Post({
    required this.q,
    required this.a,
    required this.h,

  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      q: json['q'] as String,
      a: json['a'] as String,
      h: json['h'] as String,

    );
  }
}